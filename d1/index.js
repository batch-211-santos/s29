/*

	MongoDB - Query Operators
	Expand Queries in MongoDB using Query Operators

*/




/*
	Overview:

	Query Operator
		- Definition
		- Improtance


	Types of Query Operators:
	1. Comparison Query Operators
		1.1 Greater than
		1.2 Greater than or equal
		1.3 Less than
		1.4 Less than or equal to
		1.5 Not equal to
		1.6 In

	2. Evaluation Query Operator
		2.1 Regex
			2.1.1 Case Sensitive Query
			2.1.2 Case insesityve Query

	3. Logical Queary Operators]
		3.1 OR
		3.2 AND



	Field Projection
	1. Inclusion
		1.1 Returning Specific fields in embedded documents
		1.2 Exception to the inclusion rule
		1.3 Slice Operator

	2. Exclusion
		2.1 Excluding specific fields in embedded documents



*/


/*
	What does Query Operator Mean?

	-A "Query" is a request for data from a database
	-An "Operator" is a symbol that represents an action or a process


*/





// 1. COMPARISON QUERY OPERATORS
/*
	Includes:
		- Greater than
		- Greater than or equal
		- Less than
		- Less than or equal to
		- Not equal to
		- In

	1.1 GREATER THAN: "$gt"
		- "$gt" finds documents that have fields numbers that are greater than a specified value

		- Syntax:
			db.collectionName.find({field: {$gt: value}});
*/

db.users.find({age: {$gt: 76}});


/*
1.2 GREATER THAN or EQUAL TO: "$gte"
		- "$gte" finds documents that have fields numbers that are greater than or equal to 	a specified value

		- Syntax:
			db.collectionName.find({field: {$gte: value}});
*/

db.users.find({age: {$gte: 76}});


/*
1.3 LESS THAN OPERATOR: "$lt"
		- "$lt" finds documents that have fields numbers that are less than a specified value

		- Syntax:
			db.collectionName.find({field: {$lt: value}});
*/


db.users.find({age: {$lt: 65}});


/*
1.4 LESS THAN or EQUAL TO OPERATOR: "$lte"
		- "$lte" finds documents that have fields numbers that are less than or equal to a specified value

		- Syntax:
			db.collectionName.find({field: {$lte: value}});
*/

db.users.find({age: {$lte: 65}});

/*
1.5 NOT EQUAL TO OPERATOR: "$ne"
		- "$ne" finds documents that have fields numbers that are not equal to a specified value

		- Syntax:
			db.collectionName.find({field: {$ne: value}});
*/

db.users.find({age: {$ne: 65}});

/*
1.5 IN  OPERATOR: "$in"
		- "$in" finds documents with specific match criteria on one field using different values

		- Syntax:
			db.collectionName.find({field: {$in: value}});
*/

db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({courses: {$in: ["HTML", "React"]}});

// 2. EVALUATION QUERY OPERATORS

// - Evaluation operators return data based on evaluations of either individual fields or the entire collection's documents


/*
	2.1 REGEX OPERATOR: "$regex"
		- RegEx is short for regular expression
		- They are called regular expressions because they are based on regular languages
		-RegEx is used for matching strings
		- It allows us to find documents that match a a specific string pattern using regular expressions.

	2.1.1 Case senstitive query
	Syntax:
		- db.collectionName.find({field: {$regex: 'pattern'}});


*/


db.users.find({lastName: {$regex: 'A'}});


/*

	2.1.1 Case insenstitive query
	- We can run case-sensitive queries by utilizing the "i" option
	Syntax:
		- db.collectionName.find({field: {$regex: 'pattern', $options: '$optionValue'}});


*/

db.users.find({lastName: {$regex: 'A', $options: '$i'}});


// Mini Activity

db.users.find({firstName: {$regex: 'E', $options: '$i'}});



/*
3.1 OR OPERATOR: "$or"
	- "$or" finds documents that match a single criteria from multiple provided search criteria
	- Syntax:
		-db.collectionName.find({ $or: [{fieldA: valueA}, {fieldB:valueB ]});


*/

db.users.find({ $or: [{firstName: "Neil"}, {age: "25" }]});
db.users.find({ $or: [{firstName: "Neil"}, {age: {$gt:30}}]});



/*
3.1 AND OPERATOR: "$and"
	- "$and" finds documents matching multiple criteria in a single field
	- Syntax:
		-db.collectionName.find({ $and: [{fieldA: valueA}, {fieldB:valueB ]});


*/

db.users.find({ $and: [{age: {$ne:82}}, {age: {$ne:76}}]});


// Mini Activity


db.users.find({ $and: [{firstName: {$regex: 'e'}}, {age: {$lte:30
}}]});



// No internet lol


// INCLUSION



db.users.find({firstName: "Jane"})

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		"contact.phone":1
	}
);




/*
	1.2 EXCEPTION TO THE INCLUSION RULE: Suprresing the ID field
	- Allows us to exclude the "_id" field when retrieving documents
	- When using field projection, field inclusion and exclusion may not be used at the same time
	- Excluding the "_id" field is the ONLY exception to this rule

	Syntax:
		- db.collectionName.find({criteria}, {_id:0})

*/


db.users.find (
	{

		firstName: "Jane"
	},

	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0

	}

	);


 /*
	1.3 SLICE OPERATOR: "$Slice"
	
	- "$slice" operator allows us to retrieve only 1 element that matches the search criteria.

	To demonstrate, let us first insert and view an array

 */

db.users.insert ({

	namearr: [
		{

			namea: "Juan"
		},
		{

			nameb: "tamad"
		}
	]

});

db.users.find({
	namearr:
	{
		namea: "Juan"
	}

});


// Now, let us use the slice operator

db.users.find (
	{
	"namearr": 
		{

			namea: "Juan"
		},
		{

			namearr: {$slice: 1}
		}
	]

});


// Mini Activity



db.users.find (
	{

		firstName: {$regex: 'S', $options: '$i'}
	},

	{
		firstName: 1,
		lastName: 1,
		_id: 0

	}

	);


/*

	2. EXCLUSION
		- Allows us to exclude or remove specific documents when retrieving documents
		- The value provided is zero to denote that the field is being included
		- Syntax:
			-db.collectionName.find({criteria}, field: 0)


*/

db.users.find (
	{

		firstName: "Jane"
	},
	{
		contact: 0,
		department: 0
	}


	);


/*
	
EXCLUDING/SUPPRESSING


*/

db.users.find (
	{

		firstName: "Jane"
	},
	{
		contact.phone: 0,
	}


	);